$(document).ready(function() {

	/* This is basic - uses default settings */
	
	$("a.payment-options").fancybox();

  // trigger the right arrow of mobile menu
  $(document).on("click", ".expand-menu", function() {
    $(this).closest(".mobile-menu__first-level").find(".close-dropdown").trigger("click");    
    $(this).closest(".mobile-menu__first-level").find(".mobile-submenu__list").css({"right":0});
  });
  $(document).on("click", ".back-to-menu", function(e) {
    e.preventDefault();
    //show the list items
    $('.mobile-menu__list-item').each(function() {
        if($(this).hasClass('deactive')) {
          $(this).removeClass('deactive');
        }
    });
    $(this).closest(".mobile-menu__first-level").find(".close-dropdown").trigger("click");    
    $(this).closest(".mobile-menu__first-level").find(".mobile-submenu__list").css({"right":"-100%"});
  });
	
  if ($(window).width() < 768) {
    $('.gallery__wrapper.utw').flickity({});
  }

  //$('.product-gallery__main--utw.mobile').flickity('destroy');
  var current_color = $("#current_color").val();
  var cell_length = $('.product-gallery__main--utw.mobile .product-gallery__image[data-title="'+current_color+'"]').length;
  var options = {
    wrapAround: true,
    freeScroll:true,
    adaptiveHeight: true,
    cellSelector: '[data-title="'+current_color+'"]',
    cellAlign:(cell_length >= 2)?"left":"center",
    dragThreshold: 10,
    imagesLoaded: true,
    groupCells:true,
    pageDots: true,
    prevNextButtons: true
  };
 $('.product-gallery__main--utw.mobile .product-gallery__image:not([data-title="'+current_color+'"]').hide();
 $('.product-gallery__main--utw.mobile').flickity(options);

 //Show the different product model according to the variant selection.

 $(document).on('click', '.swatch-element.color', function() {
  var selected_colour = $(this).attr('aria-label');
  $('.model-stats').each(function() {
      if(!$(this).hasClass('hidden')) {
          $(this).addClass('hidden');
      }
      if($(this).attr('data-variant-title').includes(selected_colour)) {
          if($(this).hasClass('hidden')) {
              $(this).removeClass('hidden');
          }
      }
  })
});

 //trigger mobile menu arrows
 $(document).on('click', '.mobile-menu__first-level[data-mobile-menu-has-toggle="true"] .mobile-menu__item, .expand-menu-temp', function(e) {
   e.preventDefault();
   //console.log("This is an item test");
   $('.mobile-menu__list-item').each(function() {
     if(!$(this).hasClass("deactive")) {
      $(this).addClass("deactive");
     }
   })
   $(this).closest('.mobile-menu__list-item').removeClass("deactive");
   $(this).closest('.mobile-menu__first-level').find('.expand-menu').trigger('click');
 });

  //show the corresponding color images when the swatches are clicked.
  $(document).on("click", ".swatch-element.color", function() {
    var target = $(this);    
    if($(window).width() > 767) {
      $(".product-gallery__main--utw:not(.mobile)").find(".product-gallery__image").each(function() {
        if($(this).attr("data-custom-title") == target.attr("aria-label").replace(/[\s\/]/g, '').toLowerCase()) {
            $(this).show();
        } else {
            $(this).hide();
        }
      })
    } else {
      $('.product-gallery__main--utw.mobile').flickity('destroy');
      current_color = target.attr("aria-label").replace(/ /g,'').replace(/[\s\/]/g, '').toLowerCase();
      var options = {
        wrapAround: true,
        adaptiveHeight: true,
        cellSelector: '[data-custom-title="'+current_color+'"]',
        dragThreshold: 10,
        imagesLoaded: true,
        pageDots: true,
        prevNextButtons: true
      };
      $('.product-gallery__main--utw.mobile .product-gallery__image[data-custom-title]').show();
      $('.product-gallery__main--utw.mobile .product-gallery__image:not([data-custom-title="'+current_color+'"]').hide();
      $('.product-gallery__main--utw.mobile').flickity(options);
    }  
  });

  //show/hide the boost filter
  $(document).on("click", ".collection__filter_trigger", function() {
    $(this).closest(".collection__filter_wrap").find(".boost-pfs-filter-tree").toggle();
  });

  //show the corresponding swatch images on the collection page.
  $(document).on('mouseenter', '.thumbnail-swatch a', function() {
    console.log("===========", $("#unisex_status").length)
    if($("#unisex_status").length == 0) {
      //do something
      console.log("==============================")
      var $swatch_image = $(this).find('span').attr('data-image');
      var image_wrap = $(this).closest('.column').find('.product-wrap').find('img');
      image_wrap.attr('data-src', $swatch_image);
      image_wrap.attr('data-srcset', $swatch_image);
      image_wrap.attr('srcset', $swatch_image);
    }    
 });

});


/* Open all external links into a new tab. */
var links = document.links;
for (let i = 0, linksLength = links.length ; i < linksLength ; i++) {
  if (links[i].hostname !== window.location.hostname) {
    links[i].target = '_blank';
    links[i].rel = 'noreferrer noopener';
  }
}